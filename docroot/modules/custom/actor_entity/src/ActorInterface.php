<?php

namespace Drupal\actor_entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Actor entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup content_entity
 */
interface ActorInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
