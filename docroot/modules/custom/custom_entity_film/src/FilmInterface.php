<?php

namespace Drupal\custom_entity_film;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Film entity.
 * @ingroup custom_entity_film
 */
interface FilmInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
